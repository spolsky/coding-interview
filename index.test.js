const solution = require('./');

test('adds 1 + 2 to equal 3', () => {
  expect(solution(1, 2)).toBe(3);
});